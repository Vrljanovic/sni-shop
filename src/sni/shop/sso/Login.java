package sni.shop.sso;

import java.util.Optional;

import sni.shop.dao.UserDAO;
import sni.shop.dto.User;

public class Login {
	
	public static AuthenticationSession authSession = new AuthenticationSession();
	
	public static User AuthenticateUser(String username, String password) {
		
		User user = new UserDAO().getUserByUsername(username);
		if(user != null && user.getPassword().equals(password))
			return user;
		return null;
	}
	
	public static boolean checkPermission(String username, String app) {
		
		
		User user = new UserDAO().getUserByUsername(username);
		return checkPermission(user, app);
	}

	public static boolean checkPermission(User user, String app) {
		
		if (user == null)
			return false;
		Optional<String> key = authSession.getToken(user);
		
		if (key.isPresent()) {
			if (checkPermissionForApp(user, app)) 
				return true;
		}
		return false;

	}

	private static boolean checkPermissionForApp(User user, String app) {

		if ("Admin".equals(user.getRole()))
			return true;

		if ("ProductAdmin".equals(user.getRole()) && !"Admin".equals(app))
			return true;

		if (app.equals("Customer"))
			return true;

		return false;
	}
}

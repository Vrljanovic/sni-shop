package sni.shop.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import sni.shop.db.connection.DatabaseConnection;
import sni.shop.dto.Product;

public class ProductDAO {
	
	public List<Product> getAllProducts(){
		List<Product> products = new LinkedList<Product>();
		
		try {
			Statement statement = DatabaseConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from product");
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String code = resultSet.getString("code");
				String description = resultSet.getString("description");
				double price = resultSet.getDouble("price");
				products.add(new Product(id, code, name, description, price));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return products;
	}
	
	public boolean addProduct(Product product) {
		try {
			String sql = "insert into product values(null, ?, ?, ?, ?)";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, product.getCode());
			statement.setString(2, product.getName());
			statement.setString(3, product.getDescription());
			statement.setDouble(4, product.getPrice());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			if(resultSet.next())
				product.setId(resultSet.getInt(1));
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public Product getProductByCode(String code) {
		Product product = null;
		
		try {
			String sql = "select * from product where code = ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, code);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String description = resultSet.getString("description");
				double price = resultSet.getDouble("price");
				product = new Product(id, code, name, description, price);
			}
			resultSet.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return product;
	}
	
	public boolean removeProduct(String code) {
		try {
			String sql = "delete from product where code = ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, code);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean editProduct(Product product) {
		try {
			String sql = "update product set name = ?, description = ?, price = ? where id = ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, product.getName());
			statement.setString(2, product.getDescription());
			statement.setDouble(3, product.getPrice());
			statement.setInt(4, product.getId());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	public List<Product> searchProducts(String name){
		List<Product> products = new LinkedList<Product>();
		
		try {
			String sql = "select * from product where name like ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, "%" + name + "%");
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String pName = resultSet.getString("name");
				String code = resultSet.getString("code");
				String description = resultSet.getString("description");
				double price = resultSet.getDouble("price");
				products.add(new Product(id, code, pName, description, price));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return products;
		
	}
}

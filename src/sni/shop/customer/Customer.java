package sni.shop.customer;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sni.shop.attackLogger.AttackLogger;
import sni.shop.dao.ProductDAO;
import sni.shop.dto.Product;
import sni.shop.firewall.Firewall;
import sni.shop.sso.Login;

@WebServlet("/customer")
public class Customer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Customer() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!Login.checkPermission((String) request.getSession().getAttribute("username"), "Customer")) {
			AttackLogger.logAttack("Unauthorized Attempt", request.getRemoteAddr(), request.getQueryString());
			request.getSession().invalidate();
			response.sendRedirect("/");
		} else {
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(int i = 0; i < cookies.length; ++i){
					Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
				}
			}
			request.setCharacterEncoding("UTF-8");
			String name = request.getParameter("name");
			if (!Firewall.checkForAttacks(name, request.getRemoteAddr())) {
				List<Product> products = new ProductDAO().searchProducts(name);
				request.getSession().setAttribute("products", products);
			}
			response.sendRedirect(request.getContextPath() + "/customer.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

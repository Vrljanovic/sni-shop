function checkPrice() {
	var price = document["add-product-form"]["price"].value;
	if (price < 0) {
		alert("Price can not be negative");
		return false;
	}
	return true;
}
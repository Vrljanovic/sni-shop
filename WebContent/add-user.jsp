<%@page import="sni.shop.firewall.Firewall"%>
<%@page import="sni.shop.dto.User"%>
<%@page import="sni.shop.sso.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%
	String username = (String) request.getSession().getAttribute("username");
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0; i < cookies.length; ++i){
			Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
		}
	}
if (!Login.checkPermission(username, "Admin")) {
	request.getSession().invalidate();
	response.sendRedirect("/");
}
%>
<meta charset="UTF-8">
<title>Add New User</title>
<link rel="stylesheet" href="add-user.css">
<script src="add-user.js"></script>
<%
	User user = (User)request.getSession().getAttribute("user");
%>
</head>
<body>
<label>Add New User</label><br>
<form name="add-user-form" method="POST" action="admin" onsubmit="return checkPasswords()" accept-charset="UTF-8">
<label for="name">Name:</label><br>
<input type="text" id="name" name="name" value="<%= user == null ? "" : user.getName() %>" required><br>
<label for="surname">Surname:</label><br>
<input type="text" id="surname" name="surname" value="<%= user == null ? "" :  user.getSurname()%>" required><br>
<label for="username">User Name:</label><br>
<input type="text" id="username" name="username" <% if(user != null){ %> disabled <%}%> value="<%= user == null ? "" :  user.getUsername()%>" required><br>
<label for="password">Password:</label><br>
<input type="password" id="password" name="password" value="<%= user== null ? "" : user.getPassword()%>" required><br>
<label for="password2">Repeat Password:</label><br>
<input type="password" id="password2" name="password2" value="<%= user == null ? "" : user.getPassword()%>" required><br>
<label for="role">Role:</label><br>
<select id="role" name="role"  required>
<option id="Admin" <% if ( user != null && "Admin".equals(user.getRole())) { %> selected <%} %>>Admin</option>
<option id="ProductAdmin" <% if ( user != null && "ProductAdmin".equals(user.getRole())) { %> selected <%} %>>ProductAdmin</option>
<option id="Customer" <% if ( user != null && "Customer".equals(user.getRole())) { %> selected <%} %>>Customer</option>
</select><br><br>
<input type="submit" value="Confirm"><br>
<label id="error"><%= request.getSession().getAttribute("error-message") == null? "" : request.getSession().getAttribute("error-message")%></label>
</form>
</body>
</html>
package sni.shop.db.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	private static Connection connection;
	
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		if (connection == null)
			return connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sni_shop?serverTimezone=UTC","root","nemanja96");
		return connection;
	}
	
	
}

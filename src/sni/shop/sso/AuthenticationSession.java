package sni.shop.sso;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import sni.shop.dto.User;

public class AuthenticationSession {
	
    private Map<String, Authentication> authenticated = new HashMap<>();;

    public AuthenticationSession putAuthenticated(String key, Authentication auth) {

        authenticated.put(key, auth);
        return this;

    }

    public AuthenticationSession removeAuthenticated(String key, Authentication auth) {

        authenticated.remove(key, auth);
        return this;

    }

    public Map<String, Authentication> getAuthenticated() {
        return authenticated;
    }

    public Optional<String> getToken(User user) {

        for (String key : authenticated.keySet()) {

        	Authentication auth = authenticated.get(key);
            if (auth.getUser().getUsername().equals(user.getUsername())) {
                return Optional.of(key);
            }

        }

        return Optional.empty();
    }

}

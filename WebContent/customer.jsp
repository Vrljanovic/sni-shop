<%@page import="sni.shop.firewall.Firewall"%>
<%@page import="sni.shop.dao.ProductDAO"%>
<%@page import="sni.shop.dto.Product"%>
<%@page import="java.util.List"%>
<%@page import="sni.shop.sso.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%
	String username = (String) request.getSession().getAttribute("username");
Cookie[] cookies = request.getCookies();
if(cookies != null){
	for(int i = 0; i < cookies.length; ++i){
		Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
	}
}
	if (!Login.checkPermission(username, "Customer")){
		request.getSession().invalidate();
		response.sendRedirect("/");
	}
%>
<meta charset="UTF-8">
<title>SNI Shop - Customer</title>
<link rel="stylesheet" href="customer.css">
</head>
<body>
	
		<label id=hello-message>Welcome to SNI Shop Customer Page</label>
	<div id=current-user>
			<ul>
				<% if (Login.checkPermission(username, "Admin")) {%>
				<li><a href="admin.jsp">Admin App</a></li> <%} %>
				<% if (Login.checkPermission(username, "ProductAdmin")) {%>
				<li><a href="product-admin.jsp">Product Admin App</a></li><%} %>
				<li><a href="logout">Logout (<%= username %>)</a><li>
			</ul>
		</div>
	<br>
	<br>
	<br>
	<br>
	<button id="show-all-button" onclick="redirect()">Show All Products</button>
	<form method="GET" action="customer" id="search">
		<input type="text" height=30px name="name" required>
		<input type="submit" value="Search" id="search-button">
	</form>
	<table class="product-table">
		<thead>
			<tr id="product-table-top">
				<th>
					<h3>Code</h3>
				</th>
				<th>
					<h3>Name</h3>
				</th>
				<th>
					<h3>Description</h3>
				</th>
				<th>
					<h3>Price</h3>
				</th>
			</tr>
		</thead>
		<tbody>
			<%
				List<Product> products = (List<Product>)request.getSession().getAttribute("products");
				if(products == null)
					products = new ProductDAO().getAllProducts();
			for (Product product : products) {
			%><tr>
				<td><%=product.getCode()%></td>
				<td><%=product.getName()%></td>
				<td><%=product.getDescription()%></td>
				<td><%=product.getPrice()%></td>
				<%
					}
					request.getSession().removeAttribute("products");
				%>
			
		</tbody>
	</table>
</body>
<script>
function redirect() {
	window.location.replace("customer.jsp");
}
</script>
</html>
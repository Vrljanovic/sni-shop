<%@page import="sni.shop.firewall.Firewall"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <!DOCTYPE html>
    <html>
    <%
    	Firewall.addConnection(request.getRemoteAddr());
    	String username = (String) request.getSession().getAttribute("username");
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){
    		for(int i = 0; i < cookies.length; ++i){
    			Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
    		}
    	}
    	if(username != null){
    		String role = (String) request.getSession().getAttribute("role");
    		if ("Admin".equals(role)) {
				response.sendRedirect(request.getContextPath() + "/admin.jsp");
			} else if ("ProductAdmin".equals(role)){
				response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
			}
			else {
				response.sendRedirect(request.getContextPath() + "/customer.jsp");
			}
    	}
    %>
    <head>
        <meta charset="UTF-8">
        <title>SNI Shop</title>
        <link rel="stylesheet" href="login.css" type="text/css">
    </head>

    <body>
        <div class="login">
            <h1>SNI Shop</h1>
            <form method="post" action="login">
                <input type="text" name="username" id="username" placeholder="Username" required="required" />
                <input type="password" name="password" id="password" placeholder="Password" required="required" />
                <button class="btn btn-primary btn-block btn-large">Login</button>
            </form>
        </div>
    </body>

    </html>
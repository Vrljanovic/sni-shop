package sni.shop.attackLogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AttackLogger {

	private static final String FILENAME = "attacks.log";
	private static final String FILEROOT = System.getProperty("user.home") + File.separator + "Desktop";
	private static final SimpleDateFormat FORMATTER= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
	
	public static void logAttack(String attack, String address, String text) {
		try {
			FileWriter writer = new FileWriter(new File(FILEROOT + File.separator + FILENAME), true);
			writer.write("Attack: " + attack + System.lineSeparator());
			writer.write("Address: " + address + System.lineSeparator());
			Date date = new Date(System.currentTimeMillis());
			writer.write("Time: " + FORMATTER.format(date) + System.lineSeparator());
			writer.write("Attack Text: " + text + System.lineSeparator());
			writer.write("===========================================================================" + System.lineSeparator());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

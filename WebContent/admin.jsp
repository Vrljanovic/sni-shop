<%@page import="java.util.stream.Stream"%>
<%@page import="sni.shop.firewall.Firewall"%>
<%@page import="sni.shop.dao.UserDAO"%>
<%@page import="sni.shop.dto.User"%>
<%@page import="java.util.List"%>
<%@page import="sni.shop.sso.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<%
	
	String username = (String) request.getSession().getAttribute("username");
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0; i < cookies.length; ++i){
			Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
		}
	}
if (!Login.checkPermission(username, "Admin")) {
	request.getSession().invalidate();
	response.sendRedirect("/");
	return;
}
	request.getSession().removeAttribute("user");
%>
<meta charset="UTF-8">
<title>SNI Shop - Admin</title>
<link rel="stylesheet" href="admin.css">
<script type="text/javascript" src="admin.js"></script>
</head>
<body>
	<div>
		<label id=hello-message>Welcome to SNI Shop Admin Page</label>
		<div id=current-user>
			<ul>
				<li><a href="product-admin.jsp">Product Admin App</a></li>
				<li><a href="customer.jsp">Customer App</a></li>
				<li><a href="logout">Logout (<%= username %>)</a><li>
			</ul>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
		<button id="add-user-button" onclick="redirect()">Add New User</button>
	<table class="user-table">
		<thead>
			<tr id="user-table-top">
				<th>
					<h3>Name</h3>
				</th>
				<th>
					<h3>Surname</h3>
				</th>
				<th>
					<h3>User Name</h3>
				</th>
				<th>
					<h3>Role</h3>
				</th>
				<th>
					<h3>Edit user</h3>
				</th>
				<th>
					<h3>Delete</h3>
				</th>
			</tr>
		</thead>
		<tbody>
			<%
				List<User> users = new UserDAO().getAllUsers();
			for (User user : users) {
			%><tr>
				<td><%=user.getName()%></td>
				<td><%=user.getSurname()%></td>
				<td><%=user.getUsername()%></td>
				<td><%=user.getRole()%></td>
				<td><form method="GET" action="admin">
						<input type="hidden" name="username" value="<%= user.getUsername() %>">
						<input type="submit" name="method" class="edit-button" value="Edit">
					</form></td>
				<td><form method="GET" action="admin">
						<input type="hidden" name="username" value="<%= user.getUsername() %>">
						<input type="submit" name="method"  class="delete-button" value="Delete">
					</form></td>
				<%
					}
				%>
			
		</tbody>
	</table>
</body>
</html>
<%@page import="sni.shop.firewall.Firewall"%>
<%@page import="sni.shop.dao.ProductDAO"%>
<%@page import="sni.shop.dto.Product"%>
<%@page import="java.util.List"%>
<%@page import="sni.shop.sso.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%
	Cookie[] cookies = request.getCookies();
if(cookies != null){
	for(int i = 0; i < cookies.length; ++i){
		Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
	}
}
	String username = (String) request.getSession().getAttribute("username");
	if (!Login.checkPermission(username, "ProductAdmin")){
		request.getSession().invalidate();
		response.sendRedirect("/");
		return;
	}
	
	request.getSession().removeAttribute("product");
%>
<meta charset="UTF-8">
<title>SNI Shop - Product Admin</title>
<link rel="stylesheet" href="product-admin.css">
<script src="product-admin.js"></script>
</head>
<body>
	
		<label id=hello-message>Welcome to SNI Shop Product Admin Page</label>
		<div id=current-user>
			<ul>
				<% if (Login.checkPermission(username, "Admin")) {%>
				<li><a href="admin.jsp">Admin App</a></li> <%} %>
				<li><a href="customer.jsp">Customer App</a></li>
				<li><a href="logout">Logout (<%= username %>)</a><li>
			</ul>
		</div>
	<br>
	<br>
	<br>
	<br>
		<button id="add-product-button" onclick="redirect()">Add New Product</button>
	<table class="product-table">
		<thead>
			<tr id="product-table-top">
				<th>
					<h3>Code</h3>
				</th>
				<th>
					<h3>Name</h3>
				</th>
				<th>
					<h3>Description</h3>
				</th>
				<th>
					<h3>Price</h3>
				</th>
				<th>
					<h3>Edit Product</h3>
				</th>
				<th>
					<h3>Delete Product</h3>
				</th>
			</tr>
		</thead>
		<tbody>
			<%
				List<Product> products = new ProductDAO().getAllProducts();
			for (Product product : products) {
			%><tr>
				<td><%=product.getCode()%></td>
				<td><%=product.getName()%></td>
				<td><%=product.getDescription()%></td>
				<td><%=product.getPrice()%></td>
				<td><form method="GET" action="product-admin">
						<input type="hidden" name="code" value="<%= product.getCode() %>">
						<input type="submit" name="method" class="edit-button" value="Edit">
					</form></td>
				<td><form method="GET" action="product-admin">
						<input type="hidden" name="code" value="<%= product.getCode() %>">
						<input type="submit" name="method"  class="delete-button" value="Delete">
					</form></td>
				<%
					}
				%>
			
		</tbody>
	</table>
</body>
</html>
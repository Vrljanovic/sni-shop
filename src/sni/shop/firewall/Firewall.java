package sni.shop.firewall;

import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sni.shop.attackLogger.AttackLogger;

public class Firewall {
	private static ConcurrentHashMap<String, Integer> connections = new ConcurrentHashMap<String, Integer>();

	static {
		new Thread(() -> {
			try {
				while (true) {
					Thread.sleep(20000);
					for (String address : connections.keySet()) {
						if (connections.get(address) > 50) {
							AttackLogger.logAttack("DOS", address, "");
						}
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}).start();
	}

	public static void addConnection(String address) {
		if (connections.containsKey(address)) {
			System.out.println(connections);
			int val = connections.get(address);
			connections.remove(address);
			connections.put(address, val + 1);
		} else
			connections.put(address, 1);
	}

	public static boolean checkForAttacks(String input, String address) {
		return checkForMySQLInjection(input, address) || checkForXSS(input, address);
	}

	private static boolean checkForMySQLInjection(String input, String address) {
		String SQLIDetectionRegex = "select * from|(and|or) \\w+ *(<|>|<=|>=|=) *\\w+|(and|or) \\d+ *(<|>|<=|>=|=) *\\d+|isnull|delete from|update .*? set"
				+ "|insert into .*? values|insert into .*? (.*?) values|drop table|alter table|drop schema|union select";

		Pattern pattern = Pattern.compile(SQLIDetectionRegex);
		Matcher matcher = pattern.matcher(input.toLowerCase());
		if (matcher.find()) {
			AttackLogger.logAttack("SQL Injection", address, input);
			return true;
		}

		return false;
	}

	private static boolean checkForXSS(String input, String address) {
		String xssDetectionRegex = "<(|\\/|[^\\/>][^>]+|\\/[^>][^>]+)>";
		Pattern pattern = Pattern.compile(xssDetectionRegex);
		Matcher matcher = pattern.matcher(input.toLowerCase());
		if (matcher.find()) {
			AttackLogger.logAttack("XSS", address, input);
			return true;
		}

		return false;
	}

}

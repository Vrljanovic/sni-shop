package sni.shop.sso;

import java.util.Date;

import sni.shop.dto.User;

public class Authentication {

	private User user;
	private Date loginDate;

	public Authentication() {
	}

	public Authentication(User user, Date loginDate) {
		this.user = user;
		this.loginDate = loginDate;	
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

}

package sni.shop.login;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sni.shop.dto.User;
import sni.shop.firewall.Firewall;
import sni.shop.sso.Authentication;
import sni.shop.sso.TokenUtils;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Cookie[] cookies = request.getCookies();
		for(Cookie cookie : cookies)
			Firewall.checkForAttacks(cookie.getValue(), request.getRemoteAddr());
		if (Firewall.checkForAttacks(username, request.getRemoteAddr())) {
			response.sendRedirect("/");
		} else if (Firewall.checkForAttacks(password, request.getRemoteAddr())) {
			response.sendRedirect("/");
		} else {
			User user = sni.shop.sso.Login.AuthenticateUser(username, password);
			if (user != null) {
				request.getSession().setAttribute("username", user.getUsername());
				request.getSession().setAttribute("role", user.getRole());
				String token = TokenUtils.generateToken();
				sni.shop.sso.Login.authSession.putAuthenticated(token, new Authentication(user, new Date()));

				if ("Admin".equals(user.getRole())) {
					response.sendRedirect(request.getContextPath() + "/admin.jsp");
				} else if ("ProductAdmin".equals(user.getRole()))
					response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
				else
					response.sendRedirect(request.getContextPath() + "/customer.jsp");
			} else {
				response.sendRedirect("/");
			}
		}
	}

}

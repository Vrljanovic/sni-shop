package sni.shop.productAdmin;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sni.shop.attackLogger.AttackLogger;
import sni.shop.dao.ProductDAO;
import sni.shop.dto.Product;
import sni.shop.firewall.Firewall;
import sni.shop.sso.Login;

@WebServlet("/product-admin")
public class ProductAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ProductAdmin() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!Login.checkPermission((String) request.getSession().getAttribute("username"), "ProductAdmin")) {
			AttackLogger.logAttack("Unauthorized Attempt", request.getRemoteAddr(), request.getQueryString());
			request.getSession().invalidate();
			response.sendRedirect("/");
		} else {
			request.setCharacterEncoding("UTF-8");
			String method = request.getParameter("method");
			String code = request.getParameter("code");
			ProductDAO productDAO = new ProductDAO();
			Product product = productDAO.getProductByCode(code);
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(int i = 0; i < cookies.length; ++i){
					Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
				}
			}
			if(Firewall.checkForAttacks(code, request.getRemoteAddr())){
				response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
			}
			if (product == null) {
				AttackLogger.logAttack("Parameter Tampering", request.getRemoteAddr(), request.getQueryString());
				response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
			}
			if ("Delete".equals(method)) {
				productDAO.removeProduct(code);
				response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
			} else {
				request.getSession().setAttribute("product", product);
				response.sendRedirect(request.getContextPath() + "/add-product.jsp");
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!Login.checkPermission((String) request.getSession().getAttribute("username"), "ProductAdmin")) {
			AttackLogger.logAttack("Unauthorized Attempt", request.getRemoteAddr(), request.getQueryString());
			request.getSession().invalidate();
			response.sendRedirect("/");
		} else {
			request.setCharacterEncoding("UTF-8");
			String name = new String(request.getParameter("name").getBytes(), StandardCharsets.UTF_8);
			String description = new String(request.getParameter("description").getBytes(), StandardCharsets.UTF_8);
			double price = Double.parseDouble(request.getParameter("price"));
			String code;
			if (request.getSession().getAttribute("product") == null)
				code = new String(request.getParameter("code").getBytes(), StandardCharsets.UTF_8);
			else
				code = ((Product) request.getSession().getAttribute("product")).getCode();
			ProductDAO productDAO = new ProductDAO();
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(int i = 0; i < cookies.length; ++i){
					Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
				}
			}
			if (Firewall.checkForAttacks(name, request.getRemoteHost()) || Firewall.checkForAttacks(description, request.getRemoteHost())) {
				response.sendRedirect(request.getContextPath() + "/add-product.jsp");

			} else {

				if (productDAO.getProductByCode(code) == null) {
					Product product = new Product(null, code, name, description, price);
					if (productDAO.addProduct(product)) {
						response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
					} else {
						request.getSession().setAttribute("error-message", "Could not add new product.");
						response.sendRedirect(request.getContextPath() + "/add-product.jsp");
					}
				} else {
					Product product = (Product) request.getSession().getAttribute("product");
					product.setName(name);
					product.setDescription(description);
					product.setPrice(price);
					if (productDAO.editProduct(product)) {
						request.getSession().removeAttribute("product");
						response.sendRedirect(request.getContextPath() + "/product-admin.jsp");
					} else {
						request.getSession().setAttribute("error-message", "Could not edit product.");
						request.getSession().removeAttribute("product");
						response.sendRedirect(request.getContextPath() + "/add-product.jsp");
					}
				}
			}
		}
	}

}

package sni.shop.dto;

public class User {

	private Integer id;
	private String name;
	private String surname;
	private String username;
	private String password;
	private String role;
	
	public User(Integer id, String name, String surname, String username, String password, String role) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	
	public User() {
		this.id = null;
		this.name = "";
		this.surname = "";
		this.username = "";
		this.password = "";
		this.role = "Customer";
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public String getRole() {
		return this.role;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + System.lineSeparator() +
				"Name: " + this.name + System.lineSeparator() +
				"Surname: " + this.surname + System.lineSeparator() +
				"Username: " + this.username + System.lineSeparator() +
				"Password: " + this.password + System.lineSeparator() +
				"Role: " + this.role + System.lineSeparator();
	}
}

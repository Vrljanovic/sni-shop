package sni.shop.admin;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sni.shop.attackLogger.AttackLogger;
import sni.shop.dao.UserDAO;
import sni.shop.dto.User;
import sni.shop.firewall.Firewall;
import sni.shop.sso.Login;

@WebServlet("/admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!Login.checkPermission((String) request.getSession().getAttribute("username"), "Admin")) {
			AttackLogger.logAttack("Unauthorized Attempt", request.getRemoteAddr(), request.getQueryString());
			request.getSession().invalidate();
			response.sendRedirect("/");
		} else {
			request.setCharacterEncoding("UTF-8");
			String method = request.getParameter("method");
			String username = request.getParameter("username");
			UserDAO userDAO = new UserDAO();
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(int i = 0; i < cookies.length; ++i){
					Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
				}
			}
			User user = userDAO.getUserByUsername(username);
			if(Firewall.checkForAttacks(username, request.getRemoteAddr())) {
				response.sendRedirect(request.getContextPath() + "/admin.jsp");
			}
			if (user == null) {
				AttackLogger.logAttack("Parameter Tampering", request.getRemoteAddr(), request.getQueryString());
				response.sendRedirect(request.getContextPath() + "/admin.jsp");
			} else {
				if ("Delete".equals(method)) {
					userDAO.removeUser(username);
					response.sendRedirect(request.getContextPath() + "/admin.jsp");
				} else {
					request.getSession().setAttribute("user", user);
					response.sendRedirect(request.getContextPath() + "/add-user.jsp");
				}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!Login.checkPermission((String) request.getSession().getAttribute("username"), "Admin")) {
			AttackLogger.logAttack("Unauthorized Attempt", request.getRemoteAddr(), request.getQueryString());
			request.getSession().invalidate();
			response.sendRedirect("/");
		} else {
			request.setCharacterEncoding("UTF-8");
			String name = new String(request.getParameter("name").getBytes(), StandardCharsets.UTF_8);
			String surname = new String(request.getParameter("surname").getBytes(), StandardCharsets.UTF_8);
			String username;
			if (request.getSession().getAttribute("user") == null)
				username = new String(request.getParameter("username").getBytes(), StandardCharsets.UTF_8);
			else
				username = ((User) request.getSession().getAttribute("user")).getUsername();
			String password = new String(request.getParameter("password").getBytes(), StandardCharsets.UTF_8);
			String role = ((String) request.getParameter("role")).replace(' ', '\0');
			UserDAO userDAO = new UserDAO();
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(int i = 0; i < cookies.length; ++i){
					Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
				}
			}
			if (Firewall.checkForAttacks(name, request.getRemoteAddr())
					|| Firewall.checkForAttacks(surname, request.getRemoteAddr())
					|| Firewall.checkForAttacks(username, request.getRemoteAddr())
					|| Firewall.checkForAttacks(password, request.getRemoteAddr())) {
				request.getSession().setAttribute("error-message", "Invalid request");
				response.sendRedirect(request.getContextPath() + "/add-user.jsp");

			} else {

				if (userDAO.getUserByUsername(username) == null) {
					User user = new User(null, name, surname, username, password, role);
					if (userDAO.addUser(user)) {
						response.sendRedirect(request.getContextPath() + "/admin.jsp");
					} else {
						request.getSession().setAttribute("error-message", "Could not add new user.");
						response.sendRedirect(request.getContextPath() + "/add-user.jsp");
					}
				} else {
					User user = (User) request.getSession().getAttribute("user");
					user.setName(name);
					user.setSurname(surname);
					user.setPassword(password);
					user.setRole(role);
					if (userDAO.editUser(user)) {
						request.getSession().removeAttribute("user");
						response.sendRedirect(request.getContextPath() + "/admin.jsp");
					} else {
						request.getSession().setAttribute("error-message", "Could not edit user.");
						response.sendRedirect(request.getContextPath() + "/add-user.jsp");
					}
				}
			}
		}
	}

}

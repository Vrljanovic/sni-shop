<%@page import="sni.shop.firewall.Firewall"%>
<%@page import="sni.shop.dto.Product"%>
<%@page import="sni.shop.sso.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%
	Cookie[] cookies = request.getCookies();
if(cookies != null){
	for(int i = 0; i < cookies.length; ++i){
		Firewall.checkForAttacks(cookies[i].getValue(), request.getRemoteAddr());
	}
}
	String username = (String) request.getSession().getAttribute("username");
if (!Login.checkPermission(username, "ProductAdmin")) {
	request.getSession().invalidate();
	response.sendRedirect("/");
}
%>
<meta charset="UTF-8">
<title>Add New Product</title>
<link rel="stylesheet" href="add-product.css">
<%
	Product product = (Product)request.getSession().getAttribute("product");
%>
</head>
<body>
<label>Add New Product</label><br>
<form name="add-product-form" method="POST" action="product-admin" onsubmit="return checkPasswords()" accept-charset="UTF-8">
<label for="code">Code:</label><br>
<input type="text" id="code" name="code" <% if(product != null){ %> disabled <%}%> value="<%= product == null ? "" : product.getCode() %>" required><br>
<label for="name">Name:</label><br>
<input type="text" id="name" name="name" value="<%= product == null ? "" : product.getName() %>" required><br>
<label for="description">Description:</label><br>
<input type="text" id="description" name="description" value="<%= product == null ? "" :  product.getDescription()%>" required><br>
<label for="price">Price:</label><br>
<input type="number" id="price" name="price" step="any" min="0" value="<%= product == null ? "" :  product.getPrice()%>" required><br><br>
<input type="submit" value="Confirm"><br>
<label id="error"><%= request.getSession().getAttribute("error-message") == null? "" : request.getSession().getAttribute("error-message")%></label>
</form>
</body>
</html>
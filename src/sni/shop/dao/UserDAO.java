package sni.shop.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import sni.shop.db.connection.DatabaseConnection;
import sni.shop.dto.User;

public class UserDAO {
	
	public List<User> getAllUsers() {
		List<User> users = new LinkedList<>();
		
		try {
			Statement statement = DatabaseConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from user");
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String username = resultSet.getString("username");
				String password = resultSet.getString("password");
				String role = resultSet.getString("role");
				users.add(new User(id, name, surname, username, password, role));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return users;
	}
	
	public User getUserByUsername(String username) {
		User user = null;
		
		try {
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement("select id, name, surname, password, role from user where username = ?");
			statement.setString(1,  username);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String password = resultSet.getString("password");
				String role = resultSet.getString("role");
				user = new User(id, name, surname, username, password, role);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	public boolean addUser(User user) {
		try {
			String sql = "insert into user values(null, ?, ?, ?, ?, ?)";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getUsername());
			statement.setString(4, user.getPassword());
			statement.setString(5, user.getRole());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			if(resultSet.next())
				user.setId(resultSet.getInt(1));
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean removeUser(String username) {
		try {
			String sql = "delete from user where username = ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, username);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean editUser(User user) {
		try {
			String sql = "update user set name = ?, surname = ?, password = ?, role = ? where id = ?";
			PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sql);
			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getPassword());
			statement.setString(4, user.getRole());
			statement.setInt(5, user.getId());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
}

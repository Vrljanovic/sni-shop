function checkPasswords() {
	var pass = document.forms["add-user-form"]["password"].value;
	var pass2 = document.forms["add-user-form"]["password2"].value;
	console.log("Pass: " + pass);
	console.log("Pass2: " + pass2);
	if (pass !== pass2) {
		alert("Passwords do not match");
		return false;
	}
	if(pass.length < 4){
		alert("Passwords must have at least 4 characters.");
		return false;
	}
	return true;
}